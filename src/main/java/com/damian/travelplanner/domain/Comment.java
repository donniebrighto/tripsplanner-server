package com.damian.travelplanner.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    private String text;

    @ManyToOne
    @JoinColumn(name = "trip_point_id", referencedColumnName = "id")
    private TripPoint tripPoint;

    @OneToMany
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    private List<Comment> subComments;

    @ManyToMany
    @JoinTable(
            name = "users_liked_comment",
            joinColumns = @JoinColumn(name = "comment_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> likedBy;

}
