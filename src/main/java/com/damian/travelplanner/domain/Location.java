package com.damian.travelplanner.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@Data
public class Location {

    @Id
    @GeneratedValue
    private long id;
    private String label;
    private String locationId;
    private String iso2flag;

}
