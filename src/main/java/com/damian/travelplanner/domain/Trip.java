package com.damian.travelplanner.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
public class Trip {

    @Id
    @GeneratedValue
    @Setter(AccessLevel.NONE)
    private long id;

    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    private Location destination;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "accommodation_id", referencedColumnName = "id")
    private Location accommodation;

    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    private Date endDate;

    @ManyToMany
    @JoinTable(
            name = "trip_tags",
            joinColumns = @JoinColumn(name = "trip_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tags;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL)
    private List<Membership> memberships;

    @OneToMany(mappedBy = "trip")
    @JsonManagedReference
    private List<TripPoint> tripPoints;

    @OneToOne
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    private Image image;

    public void setMemberships(List<User> members) {
        this.memberships = members.stream()
                .map(user -> new Membership(user, this))
                .collect(Collectors.toList());
    }
}
