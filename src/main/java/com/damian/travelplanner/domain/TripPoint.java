package com.damian.travelplanner.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripPoint {

    @Id
    @GeneratedValue
    private long id;

    private String googlePlaceId;

    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private User author;

    @ManyToOne
    @JoinColumn(name = "trip_id", referencedColumnName = "id")
    @JsonBackReference
    private Trip trip;

    private Date date;

    @OneToMany(mappedBy = "tripPoint")
    private List<Comment> comments;

    @ManyToMany
    @JoinTable(
            name = "users_liked_trip_point",
            joinColumns = @JoinColumn(name = "trip_point_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> likedBy;
}
