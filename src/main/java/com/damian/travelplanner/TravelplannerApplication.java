package com.damian.travelplanner;

import com.damian.travelplanner.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class TravelplannerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravelplannerApplication.class, args);
    }

}
