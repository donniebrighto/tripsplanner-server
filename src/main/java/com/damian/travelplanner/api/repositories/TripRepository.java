package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.Location;
import com.damian.travelplanner.domain.Trip;
import com.damian.travelplanner.domain.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface TripRepository extends CrudRepository<Trip, Long> {

    @Query("select t from Trip t where t.owner = :user or :user member of t.members")
    List<Trip> findByUser(@Param("user") User user);

    @Query("select t from Trip t where t.endDate < :now and (t.owner = :user or :user member of t.members)")
    List<Trip> findPastTripsForUser(
            @Param("user") User user,
            @Param("now")@DateTimeFormat(pattern = "yyyy-MM-dd") Date actualStart
    );

    @Query("select t from Trip t where t.endDate > :now and (t.owner = :user or :user member of t.members)")
    List<Trip> findFutureTripsForUser(
            @Param("user") User user,
            @Param("now")@DateTimeFormat(pattern = "yyyy-MM-dd") Date actualStart
    );

    @Query("select t from Trip t where t.startDate <= :now and t.endDate >= :now and (t.owner = :user or :user member of t.members)")
    List<Trip> findActiveTripsForUser(
            @Param("user") User user,
            @Param("now")@DateTimeFormat(pattern = "yyyy-MM-dd") Date actualStart
    );

    @Modifying
    @Query(value = "insert into trip_members (trip_id, user_id) values (:tripId, :userId)", nativeQuery = true)
    void addMemberToTrip(
            @Param("userId") long userId,
            @Param("tripId") long tripId
    );

    @Modifying
    @Query(value = "update Trip t set t.accommodation = :accommodation where t = :trip")
    void setTripAccommodation(
            @Param("accommodation") Location accommodation,
            @Param("trip") Trip trip
    );

}
