package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
