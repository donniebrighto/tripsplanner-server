package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Long> {
}
