package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmail(String email);

    List<User> findByEmailStartingWith(String query);
}
