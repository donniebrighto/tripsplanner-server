package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.Trip;
import com.damian.travelplanner.domain.TripPoint;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface TripPointRepository extends CrudRepository<TripPoint, Long> {

    List<TripPoint> findByTripAndTime(Trip trip, Date time);

}
