package com.damian.travelplanner.api.repositories;

import com.damian.travelplanner.domain.Tag;
import org.springframework.data.repository.CrudRepository;


public interface TagRepository extends CrudRepository<Tag, Long> {

    // TODO - only findAll method needed

}
