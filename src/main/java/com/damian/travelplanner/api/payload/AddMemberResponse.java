package com.damian.travelplanner.api.payload;

import com.damian.travelplanner.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class AddMemberResponse {

    private String message;
    private List<User> members;

}
