package com.damian.travelplanner.api.payload;


import com.damian.travelplanner.domain.Trip;
import com.damian.travelplanner.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class CurrentUserResponse {

    private User currentUser;
    private List<Trip> futureTrips;

}
