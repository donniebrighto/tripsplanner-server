package com.damian.travelplanner.api.payload;

import com.damian.travelplanner.domain.Trip;
import lombok.Data;

import java.util.List;

@Data
public class CreateTripRequest {

    private Trip trip;
    private List<Long> tagsIds;
    private long imageId;

}
