package com.damian.travelplanner.api.payload;

import com.damian.travelplanner.domain.Image;
import com.damian.travelplanner.domain.Location;
import com.damian.travelplanner.domain.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
@AllArgsConstructor
public class TripCardResponse {

    private long id;

    private String name;

    private Location destination;

    private long durationInDays;

    private List<Tag> tags;

    private Image image;
}
