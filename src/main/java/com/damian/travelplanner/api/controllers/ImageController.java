package com.damian.travelplanner.api.controllers;

import com.damian.travelplanner.api.payload.UploadImageResponse;
import com.damian.travelplanner.api.services.ImageStorageService;
import com.damian.travelplanner.domain.Image;
import com.damian.travelplanner.exception.FileStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
@RequestMapping("/image")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    private ImageStorageService service;

    @PostMapping("/upload")
    public ResponseEntity<UploadImageResponse> upload(@RequestParam MultipartFile file) {
        try {
            Image image = service.storeImage(file);
            return ResponseEntity.ok(new UploadImageResponse(image.getId()));
        } catch (FileStorageException e) {
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }

    @GetMapping("/{imageId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable long imageId) {
        Image dbFile = service.getImage(imageId);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline")
                .body(new ByteArrayResource(dbFile.getData()));
    }

}
