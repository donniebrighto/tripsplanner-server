package com.damian.travelplanner.api.controllers;

import com.damian.travelplanner.api.payload.AddMemberResponse;
import com.damian.travelplanner.api.payload.CreateTripRequest;
import com.damian.travelplanner.api.payload.TripCardResponse;
import com.damian.travelplanner.api.repositories.ImageRepository;
import com.damian.travelplanner.api.repositories.TagRepository;
import com.damian.travelplanner.api.repositories.TripRepository;
import com.damian.travelplanner.api.repositories.UserRepository;
import com.damian.travelplanner.domain.Tag;
import com.damian.travelplanner.domain.Trip;
import com.damian.travelplanner.domain.User;
import com.damian.travelplanner.exception.UnexpectedAuthenticationException;
import com.damian.travelplanner.security.CurrentUser;
import com.damian.travelplanner.security.UserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@RestController
@RequestMapping(("/trips"))
public class TripController {

    private static final Logger logger = LoggerFactory.getLogger(TripController.class);
    private static final Date CURRENT_DATE = new Date();

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private TagRepository tagRepository;
    
    @Autowired
    private ImageRepository imageRepository;

    @GetMapping("/my-trips")
    public List<Trip> getAllTripsForUser(@CurrentUser UserPrincipal userPrincipal) throws UnexpectedAuthenticationException {
        User user = getAuthenticatedUser(userPrincipal);
        return tripRepository.findByUser(user);
    }

    @GetMapping("/all")
    @Transactional
    public List<TripCardResponse> getAllTrips() {
        List<Trip> trips = (List<Trip>) tripRepository.findAll();
        return trips.stream()
                .map(this::mapToTripCardResponse)
                .collect(Collectors.toList());
    }

    private TripCardResponse mapToTripCardResponse(Trip trip) {
        long diffInMillis = trip.getEndDate().getTime() - trip.getStartDate().getTime();
        long diffInDays = TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);

        return TripCardResponse.builder()
                .id(trip.getId())
                .destination(trip.getDestination())
                .durationInDays(diffInDays)
                .name(trip.getName())
                .tags(trip.getTags())
                .image(trip.getImage())
                .build();
    }

    @GetMapping("/{id}")
    public Trip getTripById(@PathVariable long id) {
        // TODO - różne widoki w zależności od tego czy użytkownik należy do tej wycieczki
        return tripRepository.findById(id).orElse(null);
    }

    @GetMapping("/future")
    public List<Trip> getFutureTrips(@CurrentUser UserPrincipal userPrincipal) throws UnexpectedAuthenticationException {
        User user = getAuthenticatedUser(userPrincipal);
        return tripRepository.findFutureTripsForUser(user, CURRENT_DATE);
    }

    @GetMapping("/active")
    public List<Trip> getActiveTrips(@CurrentUser UserPrincipal userPrincipal) throws UnexpectedAuthenticationException {
        User user = getAuthenticatedUser(userPrincipal);
        return tripRepository.findActiveTripsForUser(user, CURRENT_DATE);
    }

    @GetMapping("/past")
    public List<Trip> getPastTrips(@CurrentUser UserPrincipal userPrincipal) throws UnexpectedAuthenticationException {
        User user = getAuthenticatedUser(userPrincipal);
        return tripRepository.findPastTripsForUser(user, CURRENT_DATE);
    }

    @PostMapping("/create")
    public Trip createTrip(@CurrentUser UserPrincipal userPrincipal, @RequestBody CreateTripRequest payload) throws UnexpectedAuthenticationException {
        User user = getAuthenticatedUser(userPrincipal);
        Trip trip = payload.getTrip();
        trip.setOwner(user);
        trip.setMembers(List.of(user));
        trip.setTags((List<Tag>) tagRepository.findAllById(payload.getTagsIds()));
        trip.setImage(imageRepository.findById(payload.getImageId()).orElse(null));
        return tripRepository.save(trip);
    }

    @PostMapping("/{id}/member")
    public ResponseEntity<AddMemberResponse> addMemberToTrip(@PathVariable long id, @RequestBody String email) {
        User newMember = userRepository.findByEmail(email);
        if (newMember == null) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(AddMemberResponse.builder().message("There is no user with following email: " + email).build());
        }
        Optional<Trip> tripOptional = tripRepository.findById(id);
        if (tripOptional.isEmpty()) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(AddMemberResponse.builder().message("There is no trip with following id: " + id).build());
        }
        Trip trip = tripOptional.get();
        trip.getMembers().add(newMember);
        Trip saved = tripRepository.save(trip);
        return ResponseEntity
                .ok(AddMemberResponse.builder().members(saved.getMembers()).build());
    }

    @GetMapping("/tags")
    public List<Tag> getPossibleTags() {
        return (List<Tag>) tagRepository.findAll();
    }

    private User getAuthenticatedUser(@CurrentUser UserPrincipal userPrincipal) throws UnexpectedAuthenticationException {
        Optional<User> user = userRepository.findById(userPrincipal.getId());
        if (user.isEmpty()) {
            logger.error("Unexpected Authentication State: cannot find authenticated user with id: " + userPrincipal.getId());
            throw new UnexpectedAuthenticationException();
        }
        return user.get();
    }

}
