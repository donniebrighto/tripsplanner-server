package com.damian.travelplanner.api.controllers;

import com.damian.travelplanner.api.payload.CurrentUserResponse;
import com.damian.travelplanner.api.repositories.TripRepository;
import com.damian.travelplanner.api.repositories.UserRepository;
import com.damian.travelplanner.domain.Trip;
import com.damian.travelplanner.domain.User;
import com.damian.travelplanner.exception.OAuth2AuthenticationProcessingException;
import com.damian.travelplanner.security.CurrentUser;
import com.damian.travelplanner.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TripRepository tripRepository;

    @GetMapping("/me")
    @Transactional
    public CurrentUserResponse me(@CurrentUser UserPrincipal userPrincipal) {
        User user = userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new OAuth2AuthenticationProcessingException("Nie znaleziono zalogowanego usera"));
        List<Trip> futureTripsForUser = tripRepository.findFutureTripsForUser(user, new Date());
        return new CurrentUserResponse(user, futureTripsForUser);
    }

    @GetMapping("/email")
    public List<User> userSuggestionByEmail(@CurrentUser UserPrincipal userPrincipal, @RequestParam String query) {
        return userRepository.findByEmailStartingWith(query).stream()
                .filter(user -> !Objects.equals(user.getId(), userPrincipal.getId()))
                .collect(Collectors.toList());
    }
}
