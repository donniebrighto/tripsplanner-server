package com.damian.travelplanner.security.oauth2;

public interface OAuth2UserInfo {

    String getId();
    String getName();
    String getEmail();
    String getImageUrl();

}
